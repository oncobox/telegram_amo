import json
import logging
from pathlib import Path

from telegram.ext import (
    Updater, ConversationHandler, CommandHandler,
    CallbackQueryHandler, MessageHandler, Filters
)

from amo_api import AmoApi
from bot import (
    start_command_handler, help_command_handler, start_callback_handler,
    typing_handler, show_leads_handler,
    chose_lead_handler, chose_lead_callback_handler, pdf_handler
)


def check_perms(whitelist, handler):
    def handler_wrapper(update, context):

        if update.effective_chat['type'] == 'private' and update.effective_chat['username'] in whitelist:
            return handler(update, context)

        return help_command_handler(update, context)

    return handler_wrapper


def init_bot(
        api_client,
        organization_data,
        whitelist,
        token,
        sock5_host=None,
        sock5_port=None,
        sock5_username=None,
        sock5_pass=None
):

    request_kwargs = None
    if sock5_host and sock5_port and sock5_username and sock5_pass:
        request_kwargs = {
            'proxy_url': 'socks5h://{}:{}'.format(sock5_host, sock5_port),
            'urllib3_proxy_kwargs': {
                'username': sock5_username,
                'password': sock5_pass,
            }
        }

    updater = Updater(token, use_context=True, request_kwargs=request_kwargs)
    updater.bot._api_client = api_client
    updater.bot._organization_data = organization_data
    conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler('help', help_command_handler),
            CommandHandler('start', check_perms(whitelist, start_command_handler)),

        ],
        states={
            'start_callback_handler': [CallbackQueryHandler(check_perms(whitelist, start_callback_handler))],
            'show_leads_handler': [CallbackQueryHandler(check_perms(whitelist, show_leads_handler))],
            'chose_lead_handler': [CallbackQueryHandler(check_perms(whitelist, chose_lead_handler))],
            'chose_lead_callback_handler': [CallbackQueryHandler(check_perms(whitelist, chose_lead_callback_handler))],
        },
        fallbacks=[
            CommandHandler('start', check_perms(whitelist, start_command_handler)),
        ]
    )
    updater.dispatcher.add_handler(conv_handler)
    updater.dispatcher.add_handler(MessageHandler(Filters.text, check_perms(whitelist, typing_handler)))
    updater.dispatcher.add_handler(MessageHandler(Filters.document.pdf, check_perms(whitelist, pdf_handler)))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s - %(message)s', level=logging.INFO)

    secrets_amo_fp = Path(__file__).absolute().parent / 'secrets_amo.json'
    secrets_bot_fp = Path(__file__).absolute().parent / 'secrets_bot.json'
    secrets_organization_fp = Path(__file__).absolute().parent / 'secrets_organization.json'

    with secrets_bot_fp.open() as f:
        secrets_bot = json.load(f)

    with secrets_organization_fp.open() as f:
        organization_data = json.load(f)

    init_bot(
        AmoApi(secrets_amo_fp),
        organization_data,
        secrets_bot['whitelist'],
        secrets_bot['token'],
        secrets_bot.get('sock5_host'),
        secrets_bot.get('sock5_port'),
        secrets_bot.get('sock5_username'),
        secrets_bot.get('sock5_pass')
    )
