import datetime
import json
import logging
from pathlib import Path
import urllib

import requests
from babel.dates import format_date

logger = logging.getLogger(__name__)


class ApiResponseError(Exception):
    pass


class ApiUnauthorizedError(Exception):
    pass


def hold_fresh_token(fn):

    def wrapper(self, *args, **kwargs):

        try:
            return fn(self, *args, **kwargs)
        except ApiUnauthorizedError:

            if not self.refresh_token:
                self.refresh_token = None

            self.access_token, self.refresh_token = self._get_new_tokens()
            self._dump_secrets()

            return fn(self, *args, **kwargs)

    return wrapper


class AmoApi:

    auth_url = '/oauth2/access_token'
    leads_url = '/api/v2/leads'
    account_url = '/api/v2/account'

    def __init__(self, secrets_fp):

        self.secrets_fp = Path(secrets_fp)
        self.access_token = None
        self.refresh_token = None
        self.client_id = None
        self.client_secret = None
        self.redirect_uri = None
        self.host = None

        self._load_secrets()

    def _load_secrets(self):

        with self.secrets_fp.open() as f:
            secrets = json.load(f)

        self.client_id = secrets['client_id']
        self.client_secret = secrets['client_secret']
        self.access_token = secrets['access_token']
        self.refresh_token = secrets['refresh_token']
        self.redirect_uri = secrets['redirect_uri']
        self.host = secrets['host']
        self.custom_fields_ids = secrets['custom_fields_ids']
        self.primary_custom_fields_ids = secrets['primary_custom_fields_ids']
        self.pipeline_id = secrets['pipeline_id']

        logger.debug('SUCCESS load tokens from secrets: {}'.format(secrets))

    def _dump_secrets(self):

        secrets = {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "access_token": self.access_token,
            "refresh_token": self.refresh_token,
            "redirect_uri": self.redirect_uri,
            "host": self.host,
            "custom_fields_ids": self.custom_fields_ids,
            "primary_custom_fields_ids": self.primary_custom_fields_ids,
            "pipeline_id": self.pipeline_id,
            "auth_code": ""
        }

        with self.secrets_fp.open('w') as f:
            json.dump(secrets, f)

        logger.debug('SUCCESS dump new tokens: {}'.format(secrets))

    def _get_new_tokens(self):

        auth_url = urllib.parse.urljoin(self.host, self.auth_url)
        headers = {"Content-Type": "application/json"}

        with self.secrets_fp.open() as f:
            secrets = json.load(f)

        data = {
            "grant_type": 'refresh_token',
            "client_id": self.client_id,
            "refresh_token": secrets['refresh_token'],
            'client_secret': self.client_secret,
            "redirect_uri": self.redirect_uri
        }

        logger.debug(
            '\nrefresh_token in memory: {}\nrefresh_token in secrets: {}'.format(
                self.refresh_token, secrets['refresh_token'])
        )

        response = requests.post(auth_url, json=data, headers=headers)

        if not response.status_code == 200:
            raise ApiResponseError(
                "Can't get web token - wrong status code \"{}\". Response content: {}".format(
                    response.status_code, response.content))

        response = response.json()
        logger.debug('refresh token response: {}'.format(response))

        if 'access_token' not in response:
            raise ApiResponseError(
                "Can't get web token - response has not key \"access_token\"."
                " Response: {}".format(response))

        if 'refresh_token' not in response:
            raise ApiResponseError(
                "Can't get web token - response has not key \"refresh_token\"."
                " Response: {}".format(response))

        return response['access_token'], response['refresh_token']

    def _auth_header(self):
        return {"Authorization": "Bearer {}".format(self.access_token)}

    def return_response(self, response, valid_status_codes=None, as_json=True):

        if valid_status_codes is None:
            valid_status_codes = [200]

        try:

            if response.status_code == 401:
                msg = 'Unauthorized Error. Token will be refresh.'
                logger.info(msg)
                raise ApiUnauthorizedError(msg)

            if response.status_code not in valid_status_codes:
                msg = 'Response has wrong status code. \nExpected: {}\nActual: {}\nresponse: {}'.format(
                        valid_status_codes, response.status_code, response.content.decode()[:200])
                logger.error('requested url: {}\ntokens: {} | {}\nexception: {}'.format(
                    response.url, self.access_token, self.refresh_token, msg))
                raise ApiResponseError(msg)
        except ApiResponseError as e:
            raise e
        else:
            return response.json() if as_json else response

    def make_list_url(self, url):
        return urllib.parse.urljoin(self.host, url)

    def make_item_url(self, url, item_id):
        list_url = self.make_list_url(url)
        return urllib.parse.urljoin(list_url, item_id)

    @hold_fresh_token
    def get_leads(self, query=None, status_id=None, limit_rows=None, limit_offset=None):

        filters = dict()
        if query is not None:
            filters.update({'query': query})

        if status_id is not None:
            filters.update({
                'status': status_id
            })

        if limit_rows is not None and limit_offset is not None:
            filters.update({
                'limit_rows': limit_rows,
                'limit_offset': limit_offset
            })

        url = self.make_list_url(self.leads_url)

        headers = self._auth_header()
        headers.update({"Content-Type": "application/json"})

        response = self.return_response(
            requests.get(url, params=filters, headers=headers),
            valid_status_codes=[200, 204],
            as_json=False
        )

        if response.status_code == 204:
            return []

        return [
            Lead(
                lead,
                primary_custom_fields_ids=self.primary_custom_fields_ids,
                custom_fields_ids=self.custom_fields_ids
            ) for lead in response.json()['_embedded']['items']]

    @hold_fresh_token
    def get_lead(self, _id):

        filters = {'id': _id}

        url = self.make_list_url(self.leads_url)

        headers = self._auth_header()
        headers.update({"Content-Type": "application/json"})

        response = self.return_response(
            requests.get(url, params=filters, headers=headers),
            valid_status_codes=[200, 204],
            as_json=False
        )

        if response.status_code == 204:
            return None

        return Lead(
            response.json()['_embedded']['items'][0],
            primary_custom_fields_ids=self.primary_custom_fields_ids,
            custom_fields_ids=self.custom_fields_ids
        )

    @hold_fresh_token
    def get_pipelines(self):

        params = {"with": 'pipelines'}

        url = self.make_list_url(self.account_url)

        headers = self._auth_header()
        headers.update({"Content-Type": "application/json"})

        response = self.return_response(
            requests.get(url, params=params, headers=headers),
            valid_status_codes=[200, 204],
            as_json=False
        )

        if response.status_code == 204:
            return []

        return list(response.json()['_embedded']['pipelines'].values())

    @hold_fresh_token
    def get_pipeline_statuses(self, pipeline_id):

        params = {
            "with": 'pipelines',
            'id': pipeline_id
        }

        url = self.make_list_url(self.account_url)

        headers = self._auth_header()
        headers.update({"Content-Type": "application/json"})

        response = self.return_response(
            requests.get(url, params=params, headers=headers),
            valid_status_codes=[200, 204],
            as_json=False
        )

        if response.status_code == 204:
            return []

        return response.json()['_embedded']['pipelines'][str(pipeline_id)]['statuses']

    def get_pipelines_list(self):

        pipelines = self.get_pipelines()

        if not pipelines:
            return []

        return [{'id': i['id'], 'name': i['name']} for i in pipelines]

    def get_pipeline_statuses_list(self, pipeline_id):
        statuses = self.get_pipeline_statuses(pipeline_id)

        if not statuses:
            return []

        return [{'id': i['id'], 'name': i['name']} for i in statuses.values()]

    def get_lead_by_name(self, name):
        leads = self.get_leads(query=name)

        if not leads:
            return

        return leads[0]


class Lead:

    def __init__(self, data, primary_custom_fields_ids=None, custom_fields_ids=None):
        self._data = data
        self.primary_custom_fields_ids = primary_custom_fields_ids or {}
        self.custom_fields_ids = custom_fields_ids or {}

        self._contract_id = None
        self._full_name = None
        self._first_name = None
        self._last_name = None
        self._middle_name = None
        self._address = None
        self._service_description = None
        self._sum = None
        self._inn = None
        self._kpp = None

    def _get_custom_value(self, ids):

        value = None

        for field_id in ids:
            for contact_info in self.data['custom_fields']:

                if contact_info['id'] == field_id:
                    value = contact_info['values'][0]['value']
                    break

            if value is not None:
                break

        return value

    def _set_value(self, field_name, default_value):
        primary_field_id = self.primary_custom_fields_ids.get(field_name)
        field_id = self.custom_fields_ids.get(field_name)

        setattr(self, f'_{field_name}', self._get_custom_value([primary_field_id, field_id]))

        if getattr(self, f'_{field_name}') is None:
            setattr(self, f'_{field_name}', default_value)

    @property
    def data(self):
        return self._data

    @property
    def id(self):
        return self.data['id']

    @property
    def contract_id(self):
        if self._contract_id is None:
            self._set_value('contract_id', self.data['name'])
        return self._contract_id

    @property
    def created_at(self):
        return datetime.date.fromtimestamp(self.data['created_at'])

    @property
    def full_name(self):
        if self._full_name is None:
            self._set_value('full_name', '!!!! ОШИБКА ПОЛУЧЕНИЯ ИМЕНИ КЛИЕНТА !!!!')
        return self._full_name

    @property
    def first_name(self):

        if self._first_name is None:
            full_name = self.full_name.split(' ')
            if len(full_name) > 1:
                self._first_name = full_name[1]
            else:
                self._first_name = ''

        return self._first_name

    @property
    def last_name(self):

        if self._last_name is None:
            full_name = self.full_name.split(' ')
            if len(full_name) > 0:
                self._last_name = full_name[0]
            else:
                self._last_name = ''

        return self._last_name

    @property
    def middle_name(self):

        if self._middle_name is None:
            full_name = self.full_name.split(' ')
            if len(full_name) > 2:
                self._middle_name = full_name[2]
            else:
                self._middle_name = ''

        return self._middle_name

    @property
    def address(self):
        if self._address is None:
            self._set_value('address', '')
        return self._address

    @property
    def sum(self):
        if self._sum is None:
            self._set_value('sum', self.data['sale'])
        return float(self._sum)

    @property
    def inn(self):
        if self._inn is None:
            self._set_value('inn', None)
        return self._inn

    @property
    def kpp(self):
        if self._kpp is None:
            self._set_value('kpp', None)
        return self._kpp

    @property
    def header(self):
        return f'Счет № {self.contract_id} от {format_date(datetime.date.today(), format="short", locale="ru_Ru")}'

    @property
    def payer_full_description(self):
        result = self.full_name
        if self.inn:
            result += f', ИНН {self.inn}'

        if self.kpp:
            result += f', КПП {self.kpp}'

        if self.address:
            result += f', {self.address}'

        return result

    @property
    def service_description(self):
        if self._service_description is None:
            self._set_value(
                'service_description',
                f'Оплата по договору № {self.contract_id} '
                f'от {format_date(self.created_at, format="short", locale="ru_Ru")}'
            )
        return self._service_description
