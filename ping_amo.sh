#!/bin/bash

SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd $SCRIPT_PATH
source /home/ubuntu/.venvs/bill_bot/bin/activate
python get_amo_leads.py >> ../logs/ping_amo.log 2>&1
