import requests
import json
from pathlib import Path

if __name__ == '__main__':

    with Path('secrets_amo.json').open() as f:
        amo = json.load(f)

    url = f"{amo['host']}oauth2/access_token"

    payload = {
        "client_secret": amo['client_secret'],
        "client_id": amo['client_id'],
        "grant_type": "authorization_code",
        "code": amo['auth_code'],
        "redirect_uri": amo['redirect_uri']
    }
    headers = {
        'Content-Type': "application/json",
    }

    response = requests.request("POST", url, data=json.dumps(payload), headers=headers)

    print('Response: ', response.text)
    tokens = json.loads(response.text)

    if 'access_token' not in tokens:
        print("Access token not found.\n", tokens)
        exit(1)

    amo['access_token'] = tokens['access_token']
    amo['refresh_token'] = tokens['refresh_token']
    amo['auth_code'] = ""

    with Path('secrets_amo.json').open('w') as f:
        json.dump(amo, f)

    print('Success!!!')
