from pathlib import Path
from datetime import datetime
import logging

from amo_api import AmoApi

logger = logging.getLogger(__name__)

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s - %(message)s', level=logging.DEBUG)

    secrets_amo_fp = Path(__file__).absolute().parent / 'secrets_amo.json'

    api_client = AmoApi(secrets_amo_fp)
    logging.debug(f'{datetime.now()}\n{api_client.get_pipeline_statuses_list(api_client.pipeline_id)}\n\n')
