import base64
import io
from io import BytesIO
from pathlib import Path

import cv2
import numpy as np
import pdfplumber
import pytesseract as pytesseract
from PyPDF2 import PdfFileReader, PdfFileWriter
from babel.dates import format_date
from babel.numbers import format_decimal
from jinja2 import FileSystemLoader, Environment
from pytesseract import Output
from pytils.numeral import rubles
from weasyprint import HTML
from reportlab.pdfgen import canvas

from qr_generator import get_qr


def _pretty(x):
    # to cope with -0.00
    return round(x, 2) if abs(x) >= 1e-2 else 0.0


def pluralize(value, arg='s'):
    """
    Return a plural suffix if the value is not 1. By default, use 's' as the
    suffix:

    * If value is 0, vote{{ value|pluralize }} display "votes".
    * If value is 1, vote{{ value|pluralize }} display "vote".
    * If value is 2, vote{{ value|pluralize }} display "votes".

    If an argument is provided, use that string instead:

    * If value is 0, class{{ value|pluralize:"es" }} display "classes".
    * If value is 1, class{{ value|pluralize:"es" }} display "class".
    * If value is 2, class{{ value|pluralize:"es" }} display "classes".

    If the provided argument contains a comma, use the text before the comma
    for the singular case and the text after the comma for the plural case:

    * If value is 0, cand{{ value|pluralize:"y,ies" }} display "candies".
    * If value is 1, cand{{ value|pluralize:"y,ies" }} display "candy".
    * If value is 2, cand{{ value|pluralize:"y,ies" }} display "candies".
    """
    if ',' not in arg:
        arg = ',' + arg
    bits = arg.split(',')
    if len(bits) > 2:
        return ''
    singular_suffix, plural_suffix = bits[:2]

    try:
        if float(value) != 1:
            return plural_suffix
    except ValueError:  # Invalid string that's not a number.
        pass
    except TypeError:  # Value isn't a string or a number; maybe it's a list?
        try:
            if len(value) != 1:
                return plural_suffix
        except TypeError:  # len() of unsized object.
            pass
    return singular_suffix


def pluralize_ru(value, forms):
    """
    Pluralization for Russian language.
    f(value, forms='one,two,many') returns either one, two or many.

    Some words have two forms only - singular and plural.
    In that case f(value, forms='one,many') returns either one or many.

    Based on https://gist.github.com/dpetukhov/cb82a0f4d04f7373293bdf2f491863c8
    """

    try:

        form_list = forms.split(u',')

        if len(form_list) == 2:
            one, many = form_list
            two = many
        elif len(form_list) == 3:
            one, two, many = form_list
        else:
            raise ValueError('forms should be a list of 2 or 3 comma separated elements.')

        value = str(value)[-2:]  # 314 -> 14

        if 4 < int(value) < 21:
            return many

        if value.endswith('1'):
            return one
        elif value.endswith(('2', '3', '4')):
            return two
        else:
            return many

    except (ValueError, TypeError):
        return ''


def floatformat(value, arg):
    return '{{:.{}f}}'.format(arg).format(value)


def get_template(template_path):

    env = Environment(loader=FileSystemLoader(searchpath=str(Path(__file__).parent)))
    env.filters['pluralize'] = pluralize
    env.filters['pluralize_ru'] = pluralize_ru
    env.filters['floatformat'] = floatformat
    return env.get_template(template_path)


def get_pdf(lead, organization_data, target=None, add_qr=True):

    env = Environment(loader=FileSystemLoader(searchpath=str(Path(__file__).absolute().parent / 'templates')))
    env.filters['pluralize_ru'] = pluralize_ru
    env.globals.update(format_decimal=format_decimal)
    env.globals.update(format_date=format_date)
    env.globals.update(int=int)
    env.globals.update(str=str)
    env.globals.update(rubles=rubles)
    template = env.get_template(str('bill.html'))

    logo_file = Path(__file__).parent / 'templates/assets/logo.svg'
    with logo_file.open("rb") as image_file:
        encoded_logo = base64.b64encode(image_file.read()).decode()

    encoded_qr = None
    if add_qr:
        img_file = BytesIO()
        get_qr(lead, organization_data, fmt='svg').save(img_file)
        img_file.seek(0)
        encoded_qr = base64.b64encode(img_file.read()).decode()

    stamp_file = Path(__file__).parent / 'templates/assets/stamp.png'
    with stamp_file.open("rb") as image_file:
        encoded_stamp = base64.b64encode(image_file.read()).decode()

    sign_file = Path(__file__).parent / 'templates/assets/sign.png'
    with sign_file.open("rb") as image_file:
        encoded_sign = base64.b64encode(image_file.read()).decode()

    context = {
        'lead': lead,
        'qr_svg': f'data:image/svg+xml;base64,{encoded_qr}' if encoded_qr else None,
        'organization': {
            'logo_svg': 'data:image/svg+xml;base64,{}'.format(encoded_logo),
            'stamp_png': 'data:image/png;base64,{}'.format(encoded_stamp),
            'sign_png': 'data:image/png;base64,{}'.format(encoded_sign),
            **organization_data
        }
    }

    html = template.render(context)[:]
    pdf = HTML(string=html).write_pdf(target=target)

    return pdf


def add_stamp(input_pdf, positions):

    output_file = PdfFileWriter()
    input_file = PdfFileReader(input_pdf)

    page_count = input_file.getNumPages()
    for page_number in range(page_count):
        point = positions.get(page_number, None)

        input_page = input_file.getPage(page_number)

        if point is None:
            output_file.addPage(input_page)
            continue

        x_stamp, y_stamp, x_sign, y_sign = point

        watermark = io.BytesIO()
        c = canvas.Canvas(watermark)

        rotation = input_page.get("/Rotate")
        if rotation != 90:
            rotation = ''

        c.drawImage(
            Path(__file__).parent / f'templates/assets/stamp{rotation}.png',
            x=x_stamp,
            y=y_stamp,
            width=105,
            height=105,
            mask='auto',
            preserveAspectRatio=True
        )

        c.drawImage(
            Path(__file__).parent / f'templates/assets/sign{rotation}.png',
            x=x_sign,
            y=y_sign+25,
            width=75,
            height=75,
            mask='auto',
            preserveAspectRatio=True
        )

        c.save()

        stamp = PdfFileReader(watermark)

        input_page.mergePage(stamp.getPage(0))

        output_file.addPage(input_page)

    out_pdf = io.BytesIO()
    output_file.write(out_pdf)

    return out_pdf


def get_num_pages_pdf(pdf):
    return PdfFileReader(pdf).getNumPages()


def split_position(position: str) -> tuple:
    stamp_positions, sign_positions = map(str.strip, position.split(','))
    x_stamp, y_stamp = map(str.strip, stamp_positions.split('x'))
    x_sign, y_sign = map(str.strip, sign_positions.split('x'))

    return int(x_stamp), int(y_stamp), int(x_sign), int(y_sign)


def euclidean_distance(a, b):
    return np.linalg.norm(np.array(a) - np.array(b))


def rect_distance(xl, yt, xr, yb, xl2, yt2, xr2, yb2):
    left = xr2 < xl
    right = xr < xl2
    bottom = yb2 < yt
    top = yb < yt2
    if top and left:
        return euclidean_distance((xl, yb), (xr2, yt2))
    elif left and bottom:
        return euclidean_distance((xl, yt), (xr2, yb2))
    elif bottom and right:
        return euclidean_distance((xr, yt), (xl2, yb2))
    elif right and top:
        return euclidean_distance((xr, yb), (xl2, yt2))
    elif left:
        return xl - xr2
    elif right:
        return xl2 - xr
    elif bottom:
        return yt - yb2
    elif top:
        return yt2 - yb
    else:             # rectangles intersect
        return 0


def get_stamp_point(underline_cases, signer_cases, scale=1):
    x = 0
    y = 0

    best_signer = None
    if signer_cases:
        best_signer = list(
            sorted(signer_cases, key=lambda s: s['bottom'], reverse=True))[0]

    if best_signer is None:
        return x, y

    signer_left = best_signer['x0'] / scale
    signer_right = best_signer['x1'] / scale
    signer_top = best_signer['top'] / scale
    signer_bottom = best_signer['bottom'] / scale

    best_underline = None
    best_distance = 1000
    # print('\n\nbest_signer: ', best_signer)
    for underline in sorted(underline_cases, key=lambda u: u['bottom']):
        # print('\tunderline: ', underline)

        underline_left = underline['x0'] / scale
        underline_right = underline['x1'] / scale
        underline_top = underline['top'] / scale
        underline_bottom = underline['bottom'] / scale

        dist = rect_distance(
            signer_left, signer_top, signer_right, signer_bottom,
            underline_left, underline_top, underline_right, underline_bottom,
        )

        # print('\t\tdist: ', dist)

        # print('\t\tbest pair. signer. ', best_pair['signer'])
        # print('\t\tbest pair. underline. ', best_pair['underline'])

        if dist < 200 and dist < best_distance:

            best_distance = dist
            best_underline = underline

            # horiz_diff = abs((signer['x0'] - underline['x0']) / scale)
            # vert_diff = abs((signer['bottom'] - underline['bottom']) / scale)
            # print('\t\thoriz_diff: ', horiz_diff)
            # print('\t\tvert_diff: ', vert_diff)
            #
            # if vert_diff < 50 and horiz_diff < 200:
            #     best_pair = {
            #         'signer': signer,
            #         'underline': underline,
            #         'dist': dist
            #     }

    if best_underline is not None:
        x = best_underline['x0'] / scale
        y = (best_underline['bottom'] / scale) + 50  # bottom y of underline

    return x, y


def detect_positions(stream):

    underline = '______'
    signer = 'королева'

    document = pdfplumber.open(stream)
    positions = {}
    for page_idx, page in enumerate(document.pages):

        words = page.extract_words(use_text_flow=True)

        underline_cases = []
        signer_cases = []
        for idx, w in enumerate(words):
            if underline in w['text']:
                underline_cases.append(w)

            if signer in w['text'].lower():
                signer_cases.append(w)

        if not all([underline_cases, signer_cases]):
            continue

        center = get_stamp_point(underline_cases, signer_cases)

        if not all(center):
            continue

        x_stamp, y_stamp = center

        if y_stamp < 200:
            continue

        # correction
        #     /       stamp                 /       sign                    /
        pos = x_stamp, page.height - y_stamp, x_stamp, page.height - y_stamp

        positions[page_idx] = list(map(int, pos))

    return positions


def detect_positions_image(stream):

    signer = 'королева'
    debug_result = False

    positions = {}
    document = pdfplumber.open(stream)

    stream.seek(0)
    images = extract_images(document)

    for image in images:
        nparr = np.frombuffer(image['image'].read(), np.uint8)
        img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        if debug_result:
            result = img_np.copy()

        signer_cases = []
        gray_image = cv2.cvtColor(img_np, cv2.COLOR_BGR2GRAY)
        threshold_img = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        custom_config = r'--oem 3 --psm 6'
        details = pytesseract.image_to_data(
            threshold_img, output_type=Output.DICT, config=custom_config, lang='rus')

        total_boxes = len(details['text'])
        for sequence_number in range(total_boxes):
            if int(details['conf'][sequence_number]) > 0 and signer in details['text'][sequence_number].lower():
                (x, y, w, h) = (
                    details['left'][sequence_number],
                    details['top'][sequence_number],
                    details['width'][sequence_number],
                    details['height'][sequence_number]
                )
                signer_cases.append({
                    'x0': x,
                    'x1': x + w,
                    'top': y,
                    'bottom': y + h
                })
                if debug_result:
                    result = cv2.rectangle(result, (x, y), (x + w, y + h), (0, 255, 0), 2)

        underline_cases = []
        thresh = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
        horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40, 1))
        detect_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
        cnts = cv2.findContours(detect_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            x0, top = c.min(axis=0)[0]
            x1, bottom = c.max(axis=0)[0]
            underline_cases.append({
                'x0': x0,
                'x1': x1,
                'top': top,
                'bottom': bottom,
            })
            if debug_result:
                cv2.drawContours(result, [c], -1, (36, 255, 12), 2)

        # tmp = np.array([[229, 1434], [228, 1435], [614, 1435], [614, 1434]]).reshape((4, 1, 2))
        # cv2.drawContours(result, [tmp], -1, (36, 255, 12), 2)

        if debug_result:
            cv2.namedWindow("output", cv2.WINDOW_NORMAL)  # Create window with freedom of dimensions
            cv2.imshow("output", result)  # Show image
            cv2.waitKey(0)

        center = get_stamp_point(underline_cases, signer_cases, image['scale'])

        if not all(center):
            continue

        x_stamp, y_stamp = center

        if y_stamp < 200:
            continue

        pos = (
            x_stamp,
            image['size'][1] / image['scale'] - y_stamp,
            x_stamp,
            image['size'][1] / image['scale'] - y_stamp
        )

        positions[image['page'] - 1] = list(map(int, pos))

    return positions


def extract_images(pdf):

    images = []

    for page in pdf.pages:

        page_as_image = page.to_image(resolution=256)
        img_result = io.BytesIO()
        page_as_image.original.save(img_result, 'jpeg')

        img_result.seek(0)
        images.append({
            'page': page.page_number,
            'scale': page_as_image.scale,
            'size': page_as_image.original.size,
            'image': img_result
        })

    return images


# if __name__ == '__main__':
#     # doc = Path('/home/user/projects/qrgenerator/test_data/Акт3.pdf')
#
#     for doc in Path('/home/user/projects/qrgenerator/test_data').iterdir():
#         print(doc.name)
#         with doc.open('rb') as f:
#             positions = detect_positions(f)
#
#             if not positions:
#                 print('detect_positions_image')
#                 positions = detect_positions_image(f)
#
#             out_pdf = add_stamp(f, positions)
#
#         stamped = doc.parents[1] / 'stamped' / doc.name
#         stamped.parent.mkdir(exist_ok=True, parents=True)
#         with stamped.open("wb") as f_out:
#             out_pdf.seek(0)
#             f_out.write(out_pdf.read())
