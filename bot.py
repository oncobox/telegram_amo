import io
import logging
import re
import unicodedata
from io import BytesIO

import pdfplumber
from telegram.ext import ConversationHandler

from pdf import get_pdf, add_stamp, get_num_pages_pdf, split_position, detect_positions, detect_positions_image
from telegram import InlineKeyboardButton, InlineKeyboardMarkup


logger = logging.getLogger(__name__)

LEADS_PER_PAGE = 10


def slugify(value, allow_unicode=False):
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)


def start_command_handler(update, context):
    logger.debug('!!! start_command_handler context.user_data: {}'.format(context.user_data))

    update.message.reply_text(
        'Please choose action:',
        reply_markup=InlineKeyboardMarkup([
            [InlineKeyboardButton("get the bill by patient code", callback_data='get_bill')],
            [InlineKeyboardButton("get the bill with QR-code by patient code", callback_data='get_bill_with_qr')],
            [InlineKeyboardButton("show leads", callback_data='show_leads')],
            [InlineKeyboardButton("to stamp", callback_data='to_stamp')],
            [InlineKeyboardButton("auto stamper", callback_data='auto_stamp')],
        ])
    )

    return 'start_callback_handler'


def start_callback_handler(update, context):
    logger.debug('!!! start_callback_handler context.user_data: {}'.format(context.user_data))
    query = update.callback_query

    if query.data == "get_bill":
        context.user_data['action'] = {
            'name': query.data,
            'state': 'wait_patient_code'
        }

        context.bot.edit_message_text(
            text="Please type patient code: ",
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id
        )
        return
    elif query.data == "get_bill_with_qr":

        context.user_data['action'] = {
            'name': query.data,
            'state': 'wait_patient_code'
        }

        context.bot.edit_message_text(
            text="Please type patient code: ",
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id
        )
        return
    elif query.data == "show_leads":

        statuses = context.bot._api_client.get_pipeline_statuses_list(context.bot._api_client.pipeline_id)

        statuses_buttons = [
            [InlineKeyboardButton('All', callback_data='all')]
        ]
        statuses_buttons.extend([
            [InlineKeyboardButton(s['name'], callback_data=s['id'])] for s in statuses
        ])

        context.bot.edit_message_text(
            text='Please choose status:',
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id,
            reply_markup=InlineKeyboardMarkup(statuses_buttons)
        )

        return 'show_leads_handler'
    elif query.data == "to_stamp":
        context.user_data['action'] = {
            'name': query.data,
            'state': 'wait_pdf'
        }

        context.bot.edit_message_text(
            text="Please send a pdf file: ",
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id
        )
        return
    elif query.data == "auto_stamp":
        context.user_data['action'] = {
            'name': query.data,
            'state': 'wait_pdf_auto'
        }

        context.bot.edit_message_text(
            text="Please send a pdf file: ",
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id
        )
        return
    else:
        context.bot.edit_message_text(
            text="Something went wrong. Try again.\nType /start to use this bot.",
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id
        )
        context.user_data['action'] = None
        return


def render_leads_choicer(api_client, status_id, limit_rows, limit_offset):

    leads = api_client.get_leads(
        status_id=None if status_id == 'all' else status_id,
        limit_rows=limit_rows,
        limit_offset=limit_offset
    )

    leads_buttons = []
    for lead in leads:

        leads_buttons.append([InlineKeyboardButton(
            '{} ({})'.format(lead.contract_id, lead.full_name),
            callback_data='LEAD;{}'.format(lead.id))
        ])

    control_buttons = []

    has_prev = has_next = True

    if limit_offset == 0:
        has_prev = False
        control_buttons.append(InlineKeyboardButton(' ', callback_data='IGNORE;'))
    else:
        control_buttons.append(
            InlineKeyboardButton('<', callback_data='PREV-PAGE;{};{};{}'.format(status_id, limit_rows, limit_offset))
        )

    control_buttons.append(InlineKeyboardButton(' ', callback_data='IGNORE;'))

    if len(leads) < limit_rows:
        has_next = False
        control_buttons.append(InlineKeyboardButton(' ', callback_data='IGNORE;'))
    else:
        control_buttons.append(
            InlineKeyboardButton('>', callback_data='NEXT-PAGE;{};{};{}'.format(status_id, limit_rows, limit_offset))
        )

    if has_prev or has_next:
        leads_buttons.append(control_buttons)

    return leads_buttons


def show_leads_handler(update, context):
    status_id = update.callback_query.data

    context.bot.edit_message_text(
        text='Please choose lead:',
        chat_id=update.callback_query.message.chat_id,
        message_id=update.callback_query.message.message_id,
        reply_markup=InlineKeyboardMarkup(render_leads_choicer(
            context.bot._api_client, status_id, LEADS_PER_PAGE, 0))
    )

    return 'chose_lead_handler'


def chose_lead_handler(update, context):
    query = update.callback_query
    action, *rest = query.data.split(';')
    logger.debug('!!! chose_lead_handler. action: {} | rest: {}'.format(action, rest))
    if action == "IGNORE":
        context.bot.answer_callback_query(callback_query_id=query.id)
    elif action == "PREV-PAGE":
        status_id, limit_rows, limit_offset = rest
        context.bot.edit_message_text(
            text='Please choose lead:',
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id,
            reply_markup=InlineKeyboardMarkup(render_leads_choicer(
                context.bot._api_client, status_id, int(limit_rows), int(limit_offset) - int(limit_rows)))
        )

        return 'chose_lead_handler'
    elif action == 'NEXT-PAGE':
        status_id, limit_rows, limit_offset = rest
        context.bot.edit_message_text(
            text='Please choose lead:',
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id,
            reply_markup=InlineKeyboardMarkup(render_leads_choicer(
                context.bot._api_client, status_id, int(limit_rows), int(limit_offset) + int(limit_rows)))
        )

        return 'chose_lead_handler'
    elif action == 'LEAD':
        lead_id = rest[0]
        context.bot.edit_message_text(
            text='Please choose doc type:',
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id,
            reply_markup=InlineKeyboardMarkup([
                [InlineKeyboardButton("get the bill", callback_data='get_bill;{}'.format(lead_id))],
                [InlineKeyboardButton("get the bill with QR-code", callback_data='get_bill_with_qr;{}'.format(lead_id))],
                # [InlineKeyboardButton("get pure QR-code", callback_data='get_qr;{}'.format(lead_id))]
            ])
        )

        return 'chose_lead_callback_handler'
    else:
        context.bot.answer_callback_query(callback_query_id=query.id, text="Something went wrong!")


def chose_lead_callback_handler(update, context):
    context.user_data['action'] = None
    query = update.callback_query
    action, lead_id = query.data.split(';')

    lead = context.bot._api_client.get_lead(lead_id)

    if lead is not None:
        if action == "get_bill":
            pdf_file = BytesIO()
            get_pdf(lead, context.bot._organization_data, pdf_file, add_qr=False)
            context.bot.edit_message_text(
                text=lead.full_name,
                chat_id=update.callback_query.message.chat_id,
                message_id=update.callback_query.message.message_id
            )

            pdf_file.seek(0)
            context.bot.send_document(
                chat_id=update.callback_query.message.chat_id,
                document=pdf_file,
                filename=f'{slugify(lead.contract_id, True)}.pdf',
            )
            pdf_file.close()
            logger.info(f'Bill made for lead: {lead.contract_id}')
            return
        if action == "get_bill_with_qr":
            pdf_file = BytesIO()
            get_pdf(lead, context.bot._organization_data, pdf_file)
            context.bot.edit_message_text(
                text=lead.full_name,
                chat_id=update.callback_query.message.chat_id,
                message_id=update.callback_query.message.message_id
            )

            pdf_file.seek(0)
            context.bot.send_document(
                chat_id=update.callback_query.message.chat_id,
                document=pdf_file,
                filename=f'{slugify(lead.contract_id, True)}.pdf',
            )
            pdf_file.close()
            logger.info(f'Bill with qr-code made for lead: {lead.contract_id}')
            return
        # elif action == "get_qr":
        #
        #     context.bot.edit_message_text(
        #         text=lead.full_name,
        #         chat_id=update.callback_query.message.chat_id,
        #         message_id=update.callback_query.message.message_id
        #     )
        #
        #     img = get_qr(lead,  context.bot._organization_data)
        #     img_file = BytesIO()
        #     img.save(img_file)
        #     img_file.seek(0)
        #     context.bot.send_document(
        #         chat_id=update.callback_query.message.chat_id,
        #         document=img_file,
        #         filename=f'{slugify(lead.contract_id, True)}.png',
        #     )
        #     img_file.close()
        #     logger.info(f'Qr-code made for lead: {lead.contract_id}')
        #     return

    context.bot.edit_message_text(
        text="Something went wrong. Try again.\nType /start to use this bot.",
        chat_id=update.callback_query.message.chat_id,
        message_id=update.callback_query.message.message_id
    )


def typing_handler(update, context):

    logger.debug('!!! update.callback_query: {}'.format(update.callback_query))
    logger.debug('!!! context.user_data: {}'.format(context.user_data))

    action = context.user_data.get('action', {}) or {}
    action_name = action.get('name')
    action_state = action.get('state')

    context.user_data['action'] = None

    if action_state == "wait_patient_code":
        logger.debug('!!!!! WAIT patient code')

        lead = context.bot._api_client.get_lead_by_name(update.message.text)

        if lead is None:
            context.bot.send_message(
                chat_id=update.message.chat_id,
                text=f'Patient with code "{update.message.text}" not found.',
            )
            logger.info(f'Patient with code "{update.message.text}" not found.')
            pass
        else:
            if action_name == 'get_bill':
                pdf_file = BytesIO()
                get_pdf(lead, context.bot._organization_data, pdf_file, add_qr=False)
                pdf_file.seek(0)
                context.bot.send_message(
                    text=lead.full_name or 'empty name',
                    chat_id=update.message.chat_id
                )
                context.bot.send_document(
                    chat_id=update.message.chat_id,
                    document=pdf_file,
                    filename='{}.pdf'.format(lead.contract_id),
                )
                pdf_file.close()
                logger.info(f'Bill made for lead: {lead.contract_id}')
            if action_name == 'get_bill_with_qr':
                pdf_file = BytesIO()
                get_pdf(lead, context.bot._organization_data, pdf_file)
                pdf_file.seek(0)
                context.bot.send_message(
                    text=lead.full_name or 'empty name',
                    chat_id=update.message.chat_id
                )
                context.bot.send_document(
                    chat_id=update.message.chat_id,
                    document=pdf_file,
                    filename='{}.pdf'.format(lead.contract_id),
                )
                pdf_file.close()
                logger.info(f'Bill with qr-code made for lead: {lead.contract_id}')
            # elif action_name == 'get_qr':
            #     img = get_qr(lead, context.bot._organization_data)
            #
            #     img_file = BytesIO()
            #     img.save(img_file)
            #     img_file.seek(0)
            #     context.bot.send_message(
            #         text=lead.full_name,
            #         chat_id=update.message.chat_id
            #     )
            #     context.bot.send_document(
            #         chat_id=update.message.chat_id,
            #         document=img_file,
            #         filename='{}.png'.format(lead.contract_id),
            #     )
            #     img_file.close()
            #     logger.info(f'Qr-code made for lead: {lead.contract_id}')
        return
    elif action_state == 'wait_stamp_page_number':
        context.user_data['num_page'] = update.message.text
        context.user_data['action'] = {'state': 'wait_stamp_position'}
        update.message.reply_text(
            f"Type positions for stamp and sing.\n "
            f"format: <x_stamp>x<y_stamp>,<x_sign>x<y_sign>\n"
            f"example: 220x270,330x270\n"
            f"Hit: left bottom corner - zero point\n"
            f"     x - horizontal (width  ~= 600)\n"
            f"     y - vertical   (height ~= 840)",
        )

        return 'chose_stamp_page_handler'
    elif action_state == 'wait_stamp_position':
        num_page = int(context.user_data['num_page'])
        input_pdf = context.user_data['input_pdf']
        doc_name = context.user_data['doc_name']
        del context.user_data['cnt_pages']
        del context.user_data['num_page']
        del context.user_data['input_pdf']
        del context.user_data['doc_name']
        del context.user_data['action']

        stamped_pdf = add_stamp(input_pdf, {num_page - 1: [*split_position(update.message.text)]})
        stamped_pdf.seek(0)

        # stamped_pdf.name = doc_name
        update.effective_message.reply_document(
            document=stamped_pdf, filename=doc_name, caption="Stamped document")

        return ConversationHandler.END

    logger.debug('!!!!! typing_handler ACTION not recognized')


def pdf_handler(update, context):

    logger.debug('!!! pdf_handler update.callback_query: {}'.format(update.callback_query))
    logger.debug('!!! pdf_handler context.user_data: {}'.format(context.user_data))

    action = context.user_data.get('action', {}) or {}
    action_name = action.get('name')
    action_state = action.get('state')

    if action_state == 'wait_pdf':
        logger.debug('!!!!! WAIT pdf file'.format(context.user_data))
        doc_name = update.message.document.file_name
        pdf = context.bot.getFile(update.message.document.file_id)
        input_pdf = io.BytesIO()
        pdf.download(out=input_pdf)
        cnt_pages = get_num_pages_pdf(input_pdf)
        context.user_data['action'] = {'state': 'wait_stamp_page_number'}
        context.user_data['cnt_pages'] = cnt_pages
        context.user_data['input_pdf'] = input_pdf
        context.user_data['doc_name'] = doc_name
        one_page = cnt_pages == 1
        # if one_page:
        #     return 'chose_stamp_page_handler'

        update.message.reply_text(
            f"{'There is' if one_page else 'There are'} {cnt_pages} page{'' if one_page else 's'} in document."
            f"which page to stamp?"
        )

        return 'chose_stamp_page_handler'
    if action_state == 'wait_pdf_auto':
        logger.debug('!!!!! WAIT pdf file'.format(context.user_data))
        doc_name = update.message.document.file_name
        pdf = context.bot.getFile(update.message.document.file_id)
        input_pdf = io.BytesIO()
        pdf.download(out=input_pdf)

        positions = detect_positions(input_pdf)

        if not positions:
            positions = detect_positions_image(input_pdf)

        if not positions:
            update.message.reply_text("Not found any position for stamp. Use positional stamp option.")

            return ConversationHandler.END

        stamped_pdf = add_stamp(input_pdf, positions)

        stamped_pdf.seek(0)

        # stamped_pdf.name = doc_name
        update.effective_message.reply_document(
            document=stamped_pdf, filename=doc_name, caption="Stamped document")

        return ConversationHandler.END

    logger.debug(f'!!!!! pdf_handler ACTION "{action_state}" not recognized')


def help_command_handler(update, context):
    update.message.reply_text("Type /start to use this bot.")


def error_handler(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


