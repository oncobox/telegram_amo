import logging

import qrcode
from qrcode.image.svg import SvgImage

logger = logging.getLogger(__name__)


def get_qr(lead, organisation_data, fmt='png'):

    qr = qrcode.QRCode(
        version=10,
        box_size=2,
        border=4,
    )
    qr.add_data(
        # required
        f'ST00012'
        f'|Name={organisation_data["name"]}'
        f'|PersonalAcc={organisation_data["personal_acc"]}'
        f'|BankName={organisation_data["bank_name"]}'
        f'|BIC={organisation_data["bic"]}'
        f'|CorrespAcc={organisation_data["corresp_acc"]}'
        # extra
        f'|PayeeINN={organisation_data["inn"]}'
        f'|KPP={organisation_data["kpp"]}'
        # dynamic
        f'|Sum={int(lead.sum * 100)}'
        f'|Purpose=Оплата по договору № {lead.contract_id} от {lead.created_at:%d.%m.%Y}'
        f'|LastName={lead.last_name}'
        f'|FirstName={lead.first_name}'
        f'|MiddleName={lead.middle_name}'
        f'|PayerAddress={lead.address}'
    )
    qr.make(fit=True)

    params = {
        "fill_color": 'black',
        "back_color": 'white'
    }
    if fmt == 'svg':
        params['image_factory'] = SvgImage

    return qr.make_image(**params)
